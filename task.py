import abc
import logging
import pathlib
import random
import sys
from dataclasses import dataclass

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np


@dataclass
class Params:
    seed: int
    n_vertices: int
    n_egdes: int


class DataWrapper:

    def __init__(self, params: Params):
        self.params: Params = params
        self.random = random.Random(self.params.seed)
        self.data = None

    def _create_random_edge(self, data):
        for i in range(1000):
            v1 = self.random.randint(0, self.params.n_vertices - 1)
            v2 = self.random.randint(0, self.params.n_vertices - 1)
            if v1 == v2:  # self: ignore
                continue
            if data[v1][v2] != 0:
                continue  # already exists: ignore
            return v1, v2
        raise Exception(f"Couldn't create edge after {i + 1} attempts")

    def prepare_data(self) -> 'DataWrapper':
        """
        Prepares the data for the algorithms
        """
        assert self.data is None

        data = np.zeros([self.params.n_vertices, self.params.n_vertices], dtype=int)
        for _ in range(self.params.n_egdes):
            v1, v2 = self._create_random_edge(data)
            logging.debug("v1, v2 = %s, %s", v1, v2)
            assert data[v1][v2] == 0
            assert data[v2][v1] == 0
            data[v1][v2] = data[v2][v1] = 1

        assert (data == data.transpose()).all()
        self.data = data
        return self


class Task(abc.ABC):
    def __init__(self, data_wrapper: DataWrapper):
        self.data_wrapper = data_wrapper


class Main:
    def __init__(self, params: Params):
        logging.info("seed=%s", params.seed)
        self.data_wrapper = DataWrapper(params).prepare_data()
        self.task = Task(self.data_wrapper)
        self.adjacency_list = None

    def _log_adjacency_list(self):
        logging.info("---------- Full adjacency list")
        assert self.adjacency_list
        for v1 in range(0, self.data_wrapper.params.n_vertices):
            v2s = sorted(list(self.adjacency_list[v1]))
            row_str = " ".join([str(_) for _ in v2s])
            print(f"[{v1:>3}] -> {row_str}")

    def _log_first_matix_rows(self):
        logging.info("---------- Print first 20 rows / 40 cols of matrix")
        row_str = " ".join([str(_ % 10) for _ in range(40)])
        print(f"         {row_str}")
        for row in range(len(self.data_wrapper.data[0])):
            row_str = " ".join([str(int(_)) for _ in self.data_wrapper.data[row][0:40]])
            print(f"[{row:>3}] -> {row_str} (...)")

    def _populate_adjacency_list(self):
        logging.info("---------- _populate_adjacency_list()")
        assert self.adjacency_list is None
        adjacency_list = [set() for _ in range(self.data_wrapper.params.n_vertices)]
        for row in range(len(self.data_wrapper.data[0])):
            for col in range(row + 1):
                if self.data_wrapper.data[row][col] != 0:
                    adjacency_list[row].add(col)
                    adjacency_list[col].add(row)

        self.adjacency_list = adjacency_list

    def _log_are_connected(self, v1, v2):
        are_connected = self.dfs_search(v1, v2)
        print(f"DFS - Vertices {v1} and {v2} are {'' if are_connected else 'not '}connected")

    def _log_shortest_path(self, vs, ve):
        path = self.bfs_shortest_path(vs, ve)
        print(f"BFS - Shortest path between {vs} and {ve} is {path}")

    def _get_components(self):
        components = []
        all_vertex = list(range(self.data_wrapper.params.n_vertices))
        while all_vertex:
            vertex = all_vertex.pop()
            new_component = set()
            new_component.add(vertex)
            for other_vertex in list(all_vertex):
                if self.dfs_search(vertex, other_vertex):
                    new_component.add(other_vertex)
                    all_vertex.remove(other_vertex)
                else:
                    pass
            components.append(new_component)
        return components

    def run(self):
        logging.info("---------- RUN")
        self._populate_adjacency_list()
        self._log_adjacency_list()
        self._log_first_matix_rows()

        logging.info("---------- Store matrix in data/matrix.csv")
        with pathlib.Path('data/matrix.csv').open('w') as output_file:
            for row in range(len(self.data_wrapper.data[0])):
                row_str = " ".join([str(_) for _ in self.data_wrapper.data[row]])
                output_file.write(f"{row_str}\n")

        for v1 in range(len(self.data_wrapper.data[0])):
            for v2 in range(len(self.data_wrapper.data[0])):
                are_connected = self.dfs_search(v1, v2, set())
                if not are_connected:
                    logging.debug("dfs_search(): %s and %s are NOT connected", v1, v2)

        def random_vertex():
            return self.data_wrapper.random.randint(
                0, self.data_wrapper.params.n_vertices - 1)

        # Let's check if some random vertices are connected
        v1, v2 = (random_vertex(), random_vertex())
        self._log_are_connected(v1, v2)
        self._log_shortest_path(v1, v2)

        v1, v2 = (random_vertex(), random_vertex())
        self._log_are_connected(v1, v2)
        self._log_shortest_path(v1, v2)

        v1, v2 = (random_vertex(), random_vertex())
        self._log_are_connected(v1, v2)
        self._log_shortest_path(v1, v2)

        v1, v2 = (18, 55)
        self._log_are_connected(v1, v2)
        self._log_shortest_path(v1, v2)

        # Find components
        components = self._get_components()
        assert sum([len(c) for c in components]) == self.data_wrapper.params.n_vertices
        print(f"There are {len(components)} connected components")
        for connected_component in components:
            print(connected_component)

    def dfs_search(self, v1: int, v2: int, visited_vertices: set = None) -> bool:
        if visited_vertices is None:
            visited_vertices = set()
        logging.debug("dfs_search(%s, %s)", v1, v2)
        if v2 in self.adjacency_list[v1]:
            return True
        for vertex_i in self.adjacency_list[v1]:
            if vertex_i in visited_vertices:
                continue  # ignore this vertex, was already checked
            visited_vertices.add(v1)
            if self.dfs_search(vertex_i, v2, visited_vertices):
                return True
        return False

    def bfs_shortest_path(self, v_start: int, v_end: int):
        prev = [None for _ in range(self.data_wrapper.params.n_vertices)]
        visited_vertices = set()
        queue = []  # python lists have queue semantics
        queue.append(v_start)  # enqueue

        cur = None
        while queue:
            cur = queue.pop(0)  # dequeue
            logging.debug("bfs_shortest_path() cur=%s - queue=%s", cur, queue)
            visited_vertices.add(cur)

            for vertex_next in self.adjacency_list[cur]:
                if vertex_next not in visited_vertices:
                    queue.append(vertex_next)  # enqueue
                    visited_vertices.add(vertex_next)
                    prev[vertex_next] = cur

        if prev[v_end] is None:
            return []  # path from v_start to v_end does not exists

        path_vertices = []
        path_vertex_i = v_end
        while path_vertex_i is not None:
            path_vertices.insert(0, path_vertex_i)
            path_vertex_i = prev[path_vertex_i]

        return path_vertices

    def plot(self):
        logging.info("---------- PLOT")
        assert self.data_wrapper
        assert self.adjacency_list

        # populate the graph
        graph = nx.Graph()
        for v1 in range(0, len(self.adjacency_list)):
            graph.add_node(v1)
            for v2 in self.adjacency_list[v1]:
                graph.add_edge(v1, v2)

        fig_data, ax_data = plt.subplots()

        nx.draw_networkx(graph,
                         with_labels=True,
                         ax=ax_data,
                         node_size=80,
                         width=0.5,
                         font_size=6,
                         font_color='#eee',
                         alpha=0.7)
        if not pathlib.Path('data/graph.pdf').exists():
            logging.info("Wrinting plot to data/graph.pdf")
            fig_data.savefig("data/graph.pdf")
        else:
            logging.warning("Won't write plot, file exists: data/graph.pdf")

        logging.info("---------- plot vertices 0-19")
        sg_vertices = set()
        for v1 in range(20):
            sg_vertices.update({v1})
            sg_vertices.update(self.adjacency_list[v1])
        sg = graph.subgraph(sg_vertices)

        fig_data, ax_data = plt.subplots()
        nx.draw_networkx(sg,
                         with_labels=True,
                         ax=ax_data,
                         node_size=80,
                         width=0.5,
                         font_size=6,
                         font_color='#eee',
                         alpha=0.7)

        if not pathlib.Path('data/vertices-0-19.pdf').exists():
            logging.info("Wrinting plot to data/vertices-0-19.pdf")
            fig_data.savefig("data/vertices-0-19.pdf")
        else:
            logging.warning("Won't write plot, file exists: data/vertices-0-19.pdf")

    def sample_graph(self):
        graph = nx.Graph()
        for vertex in ["P", "Q", "R", "S", "U"]:
            graph.add_node(vertex)

        for edges in [("P", "Q"), ("P", "R"), ("Q", "R"), ("S", "U")]:
            graph.add_edge(*edges)

        fig_data, ax_data = plt.subplots()
        nx.draw_random(graph,
                       with_labels=True,
                       ax=ax_data,
                       node_size=500,
                       width=0.5,
                       font_size=10,
                       font_color='#eee',
                       alpha=0.7
                       )
        if not pathlib.Path('data/sample-1.pdf').exists():
            logging.info("Wrinting plot to data/sample-1.pdf")
            fig_data.savefig("data/sample-1.pdf")
        else:
            logging.warning("Won't write plot, file exists: data/sample-1.pdf")


def setup_logging(level=logging.DEBUG):
    logging.basicConfig(level=level, stream=sys.stdout)
    logging.getLogger('matplotlib').setLevel(logging.WARNING)


if __name__ == '__main__':
    setup_logging(level=logging.INFO)
    main = Main(Params(seed=98273585, n_vertices=100, n_egdes=200))
    main.run()
    main.plot()
    main.sample_graph()
    logging.info("Finished OK")
